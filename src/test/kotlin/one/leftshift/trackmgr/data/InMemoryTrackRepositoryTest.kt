package one.leftshift.trackmgr.data

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.util.*

/**
 * @author benjamin.krenn@leftshift.one - 10/11/21.
 * @since 1.0.0
 */
internal class InMemoryTrackRepositoryTest {

    @Test
    fun `test cursor based pagination no cursor`() {
        val seed = mutableMapOf<UUID, TrackEntity>(
            Entity("a"),
            Entity("b"),
            Entity("c"),
            Entity("d"),
            Entity("e"),
            Entity("f"),
            Entity("g"),
            Entity("h"),
        )
        val classUnderTest = InMemoryTrackRepository(seed)
        val result = classUnderTest.tracks(NullCursor, 5)
        Assertions.assertThat(result.items.size).isEqualTo(5)
        Assertions.assertThat(result.next).isNotInstanceOf(NullCursor::class.java)
        Assertions.assertThat(result.next as ExistingCursor).extracting { it.value }.isEqualTo("f")
    }

    @Test
    fun `test cursor based pagination simple`() {
        val seed = mutableMapOf(
            Entity("a"),
            Entity("b"),
            Entity("c"),
            Entity("d"),
            Entity("e"),
            Entity("f"),
            Entity("g"),
            Entity("h"),
        )
        val classUnderTest = InMemoryTrackRepository(seed)
        val result = classUnderTest.tracks(ExistingCursor("d"), 5)
        Assertions.assertThat(result.items.size).isEqualTo(5)
        Assertions.assertThat(result.next).isInstanceOf(NullCursor::class.java)
        Assertions.assertThat(result.prev).isInstanceOf(ExistingCursor::class.java)
        Assertions.assertThat(result.prev as ExistingCursor).extracting { it.value }.isEqualTo("a")
    }

    @Test
    fun `test cursor based pagination`() {
        val seed = mutableMapOf(
            Entity("a"),
            Entity("b"),
            Entity("c"),
            Entity("d"),
            Entity("e"),
            Entity("f"),
            Entity("g"),
            Entity("h"),
        )
        val classUnderTest = InMemoryTrackRepository(seed)
        val result = classUnderTest.tracks(ExistingCursor("g"), 5)
        Assertions.assertThat(result.items.size).isEqualTo(2)
        Assertions.assertThat(result.next).isInstanceOf(NullCursor::class.java)
        Assertions.assertThat(result.prev).isInstanceOf(ExistingCursor::class.java)
        Assertions.assertThat(result.prev as ExistingCursor).extracting { it.value }.isEqualTo("b")
    }

    private fun Entity(name: String): Pair<UUID, TrackEntity> {
        val uuid = UUID.nameUUIDFromBytes(name.toByteArray())
        return uuid to TrackEntity(name, 1.0, 2.0, OffsetDateTime.now(), null, uuid)
    }
}