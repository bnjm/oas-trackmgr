/*
 * DO NOT MODIFY - this class was auto generated by openapi-processor-spring
 *
 * 2021.5
 * 2021-10-08T14:05:57.165814Z
 * https://docs.openapiprocessor.io/spring
 */
package one.leftshift.trackmgr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LocationResource {

    @JsonProperty("latitude")
    private Double latitude;

    @JsonProperty("longitude")
    private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
