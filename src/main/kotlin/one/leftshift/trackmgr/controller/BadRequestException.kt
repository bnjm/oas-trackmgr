package one.leftshift.trackmgr.controller

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/11/21.
 * @since 1.0.0
 */
abstract class BadRequestException(message: String, cause: Throwable?) : RuntimeException(message, cause)