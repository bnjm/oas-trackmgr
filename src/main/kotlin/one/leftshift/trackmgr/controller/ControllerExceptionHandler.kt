package one.leftshift.trackmgr.controller

import one.leftshift.trackmgr.model.GenericErrorResource
import one.leftshift.trackmgr.service.ResourceNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.*

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
@RestControllerAdvice
class ControllerExceptionHandler {

    companion object {
        private val log = LoggerFactory.getLogger(this::class.java.enclosingClass)
    }

    @ExceptionHandler(ResourceNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun resourceNotFoundException(exception: ResourceNotFoundException): ResponseEntity<GenericErrorResource> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(GenericErrorResource().apply {
                this.error = exception.message
            })
    }

    @ExceptionHandler(BadRequestException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequestException(ex: BadRequestException): ResponseEntity<GenericErrorResource> {
        return ResponseEntity.of(Optional.of(GenericErrorResource().apply {
            this.error = ex.message
        }))
    }

    @ExceptionHandler(RuntimeException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun generalException(ex: RuntimeException): ResponseEntity<GenericErrorResource> {
        log.error("Caught unhandled error:", ex)
        return ResponseEntity.internalServerError().body(GenericErrorResource().apply {
            this.error = "Unknown error: ${ex.message}"
        })
    }

}