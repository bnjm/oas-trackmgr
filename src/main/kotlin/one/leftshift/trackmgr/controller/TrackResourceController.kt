package one.leftshift.trackmgr.controller

import one.leftshift.trackmgr.OasTrackManagerConfigurationProperties
import one.leftshift.trackmgr.model.CreatedTrackResponse
import one.leftshift.trackmgr.model.TrackRequest
import one.leftshift.trackmgr.model.TrackResource
import one.leftshift.trackmgr.model.TracksResource
import one.leftshift.trackmgr.service.ValidationException
import one.leftshift.trackmgr.track.TrackService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
@RestController
class TrackResourceController(
    @Autowired private val trackService: TrackService,
    @Autowired private val config: OasTrackManagerConfigurationProperties
) {

    @GetMapping(path = ["/tracks"], produces = ["application/json"])
    fun getTracks(@RequestParam cursor: String?): ResponseEntity<TracksResource>? {
        val result = trackService.provideTracks(cursor)
        val next = if (result.next != null) "${config.serviceUrl}/tracks?cursor=${result.next}" else null
        val prev = if (result.prev != null) "${config.serviceUrl}/tracks?cursor=${result.prev}" else null
        return ResponseEntity.ok(TracksResource().apply {
            this.tracks = result.items
            this._next = next
            this._prev = prev
        })
    }

    @PostMapping(path = ["/tracks"], consumes = ["application/json"], produces = ["application/json"])
    fun createTrack(@RequestBody body: TrackRequest?): ResponseEntity<CreatedTrackResponse> {
        if (body == null) throw ValidationException("track request can not be null")
        val entity = trackService.addTrack(body)
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(CreatedTrackResponse().apply {
                this.id = entity.id.toString()
                this.createdAt = entity.createdAt
            })
    }

    @PutMapping(path = ["/tracks/{trackId}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun putTrack(@PathVariable trackId: String?, @RequestBody body: TrackRequest?): ResponseEntity<Void> {
        if (body == null) throw ValidationException("track request can not be null")
        trackService.updateTrack(trackId, body)
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }

    @DeleteMapping(path = ["tracks/{trackId}"])
    fun deleteTrack(@PathVariable trackId: String?): ResponseEntity<Void> {
        trackService.removeTrackById(trackId)
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
    }

    @GetMapping(path = ["/tracks/{trackId}"], produces = ["application/json"])
    fun getTrackById(
        @PathVariable trackId: String?, @RequestParam("embed") embed: String?,
    ): ResponseEntity<TrackResource> {
        return ResponseEntity.ok(trackService.getTrackById(trackId))
    }
}