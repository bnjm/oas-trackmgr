package one.leftshift.trackmgr.service

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/11/21.
 * @since 1.0.0
 */
class GenericServiceLayerException(message: String, cause: Throwable?) : RuntimeException(message, cause)