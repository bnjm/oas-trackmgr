package one.leftshift.trackmgr.service

import one.leftshift.trackmgr.controller.BadRequestException

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/11/21.
 * @since 1.0.0
 */
class ValidationException(message: String, cause: Throwable? = null) : BadRequestException(message, cause)