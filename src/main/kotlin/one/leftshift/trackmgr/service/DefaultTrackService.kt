package one.leftshift.trackmgr.track

import one.leftshift.trackmgr.data.*
import one.leftshift.trackmgr.model.TrackRequest
import one.leftshift.trackmgr.model.TrackResource
import one.leftshift.trackmgr.service.GenericServiceLayerException
import one.leftshift.trackmgr.service.ResourceNotFoundException
import one.leftshift.trackmgr.service.ValidationException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.Base64Utils
import java.time.OffsetDateTime
import java.util.*

@Service
class DefaultTrackService(@Autowired private val repository: TrackRepository) : TrackService, InitializingBean {

    companion object {
        private val log = LoggerFactory.getLogger(DefaultTrackService::class.java)
    }

    override fun provideTracks(cursor: String?): PaginatedResult<String?, TrackResource> {
        log.info("{}", mapOf("what" to "track_svc_get_tracks"))
        val c = if (cursor == null) NullCursor else ExistingCursor(
            String(Base64Utils.decodeFromString(cursor))
        )
        return try {
            val r = repository.tracks(c, 5)
            val next = r.next.value()?.toByteArray().encodeBase64()
            val prev = r.prev.value()?.toByteArray().encodeBase64()
            PaginatedResult(next, prev, r.items.map { it.toResource() })
        } catch (e: GenericDataLayerException) {
            log.error("Failed to load resources from the database", e)
            return PaginatedResult(null, null, emptyList())
        }
    }

    override fun addTrack(request: TrackRequest): TrackResource {
        log.debug("{}", mapOf("what" to "track_svc_add_track"))
        return insertTrack(
            TrackEntity(
                request.name ?: throw ValidationException("name is missing"),
                request.location.longitude ?: throw ValidationException("longitude is missing"),
                request.location.latitude ?: throw ValidationException("latitude is missing"),
                createdAt = OffsetDateTime.now()
            )
        ).toResource()
    }

    private fun insertTrack(trackEntity: TrackEntity): TrackEntity {
        return try {
            repository.insert(trackEntity)
        } catch (e: GenericDataLayerException) {
            log.error("error", e)
            throw GenericServiceLayerException("Failed to save resource to the database", e)
        }
    }

    private fun ByteArray?.encodeBase64(): String? = if (this == null) null else Base64Utils.encodeToString(this)

    override fun getTrackById(id: String?): TrackResource {
        if (id == null) throw ResourceNotFoundException("could not find track with id $id")
        return repository.findOne(id).let { track ->
            if (track == null) throw ResourceNotFoundException("could not find track with id $id")
            track.toResource()
        }
    }

    override fun removeTrackById(id: String?): String {
        if (id == null) throw ResourceNotFoundException("could not find track with id $id")
        return repository.delete(id) ?: throw ResourceNotFoundException("could not find track with id $id")
    }

    override fun updateTrack(id: String?, request: TrackRequest): TrackResource {
        return synchronized(this) {
            val originalTrack = getTrackById(id)
            val updatedTime = OffsetDateTime.now()
            removeTrackById(id)
            insertTrack(
                TrackEntity(
                    name = request.name ?: throw ValidationException("name is missing"),
                    longitude = request.location.longitude ?: throw ValidationException("longitude is missing"),
                    latitude = request.location.latitude ?: throw ValidationException("latitude is missing"),
                    createdAt = originalTrack.createdAt,
                    updatedAt = updatedTime,
                    id = UUID.fromString(originalTrack.id)
                )
            ).toResource()
        }
    }

    override fun afterPropertiesSet() {
        this.repository.insert(
            TrackEntity(
                "Laguna Seca Raceway",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(
            TrackEntity(
                "Paul Richard Circuit",
                46.58518564946767,
                -125.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(
            TrackEntity(
                "Red Bull Ring",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(TrackEntity("Zandvoord", 36.58518564946767, -121.7534012862815, OffsetDateTime.now()))
        this.repository.insert(
            TrackEntity(
                "Nuernburg Ring",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(
            TrackEntity(
                "Donnington Park",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(TrackEntity("Brands Hatch", 36.58518564946767, -121.7534012862815, OffsetDateTime.now()))
        this.repository.insert(TrackEntity("Hungaroring", 36.58518564946767, -121.7534012862815, OffsetDateTime.now()))
        this.repository.insert(
            TrackEntity(
                "Mount Panorama",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(
            TrackEntity(
                "Kyalami Racetrack",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
        this.repository.insert(
            TrackEntity(
                "Test Track One",
                36.58518564946767,
                -121.7534012862815,
                OffsetDateTime.now()
            )
        )
    }
}