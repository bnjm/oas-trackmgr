package one.leftshift.trackmgr.track

import one.leftshift.trackmgr.data.PaginatedResult
import one.leftshift.trackmgr.model.TrackRequest
import one.leftshift.trackmgr.model.TrackResource
import one.leftshift.trackmgr.service.GenericServiceLayerException
import one.leftshift.trackmgr.service.ResourceNotFoundException

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
interface TrackService {

    /**
     * Provides a paginated result of track resources based upon the given cursor.
     * A cursor is a pointer to a specific record in a table sorted by a uniquely identified value.
     * In this case the name of the track is used as cursor value as there can not be two tracks with the same name.
     *
     * When choosing a cursor value only take fields that are:
     *  - unique
     *  - sortable
     */
    fun provideTracks(cursor: String?): PaginatedResult<String?, TrackResource>

    @Throws(GenericServiceLayerException::class)
    fun addTrack(request: TrackRequest): TrackResource

    @Throws(ResourceNotFoundException::class)
    fun getTrackById(id: String?): TrackResource

    @Throws(ResourceNotFoundException::class)
    fun removeTrackById(id: String?): String

    fun updateTrack(id: String?, request: TrackRequest): TrackResource
}