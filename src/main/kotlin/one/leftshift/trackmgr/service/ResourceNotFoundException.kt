package one.leftshift.trackmgr.service

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
class ResourceNotFoundException(message: String) : RuntimeException(message)