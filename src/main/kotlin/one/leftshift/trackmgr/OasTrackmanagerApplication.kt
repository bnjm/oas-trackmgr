package one.leftshift.trackmgr

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@SpringBootApplication
class OasTrackmanagerApplication

@Component
class ApplicationReadyEventListener {
    companion object { private val log = LoggerFactory.getLogger(this::class.java.enclosingClass)}

    @EventListener(ApplicationReadyEvent::class)
    fun print() {
        log.info("SwaggerUI available under: http://localhost:8080/swagger/index.html")
    }
}

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "track.mgr")
class OasTrackManagerConfigurationProperties {
    var serviceUrl = "http://localhost:8080"
}

fun main(args: Array<String>) {
    runApplication<OasTrackmanagerApplication>(*args)
}