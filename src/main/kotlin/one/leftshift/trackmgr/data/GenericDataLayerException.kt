package one.leftshift.trackmgr.data

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
class GenericDataLayerException(message: String) : RuntimeException(message)