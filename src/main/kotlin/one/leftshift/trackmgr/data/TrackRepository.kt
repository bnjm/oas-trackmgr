package one.leftshift.trackmgr.data

import org.springframework.stereotype.Repository
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 *
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
interface TrackRepository {

    /**
     * Returns all tracks sorted descending by the tracks name
     */
    @Throws(GenericDataLayerException::class)
    fun tracks(): List<TrackEntity>

    /**
     * Returns a paginated result starting from the given cursor
     */
    fun tracks(cursor: Cursor, pageSize: Int): PaginatedResult<Cursor, TrackEntity>

    @Throws(GenericDataLayerException::class)
    fun insert(track: TrackEntity): TrackEntity

    @Throws(GenericDataLayerException::class)
    fun findOne(id: String): TrackEntity?

    @Throws(GenericDataLayerException::class)
    fun delete(id: String): String?
}

sealed interface Cursor {
    fun value(): String?
}

data class ExistingCursor(val value: String) : Cursor {
    override fun value(): String? = value
}

object NullCursor : Cursor {
    override fun value(): String? = null
}

data class PaginatedResult<C, T>(val next: C, val prev: C, val items: List<T>)

@Repository
class InMemoryTrackRepository(private val tracks: MutableMap<UUID, TrackEntity> = ConcurrentHashMap()) :
    TrackRepository {

    override fun tracks(): List<TrackEntity> =
        tracks.values.toList()
            .sortedBy { it.name }

    override fun tracks(cursor: Cursor, pageSize: Int): PaginatedResult<Cursor, TrackEntity> {
        return when (cursor) {
            is NullCursor -> PaginatedResult(
                ExistingCursor(
                    tracks().take(pageSize + 1).last().name
                ), NullCursor, tracks().take(pageSize)
            )
            is ExistingCursor -> {
                val r = tracks()
                    .fold(listOf<TrackEntity>() to listOf<TrackEntity>()) { acc, entity ->
                        if (acc.second.isNotEmpty()) return@fold (acc.first to acc.second.plus(entity))
                        if (entity.name == cursor.value) return@fold (acc.first to acc.second.plus(entity))
                        return@fold acc.first.plus(entity) to acc.second
                    }
                val nextCursor = if (r.second.size <= pageSize) NullCursor else ExistingCursor(
                    r.second.drop(pageSize).last().name
                )
                val prevCursor =
                    if (r.first.isEmpty()) NullCursor
                    else if (r.first.size <= pageSize) ExistingCursor(tracks().first().name)
                    else ExistingCursor(r.first[(r.first.size - pageSize) % r.first.size].name)
                PaginatedResult(
                    nextCursor,
                    prevCursor,
                    r.second.take(pageSize)
                )
            }
        }
    }

    override fun insert(track: TrackEntity): TrackEntity {
        val id = UUID.nameUUIDFromBytes(track.name.toByteArray())
        if (tracks.containsKey(id)) {
            throw GenericDataLayerException("DUPLICATE ENTRY CONSTRAINT")
        }
        tracks[UUID.nameUUIDFromBytes(track.name.toByteArray())] = track
        return track
    }

    override fun findOne(id: String): TrackEntity? = tracks.values.firstOrNull { it.id.toString() == id }
    override fun delete(id: String): String? {
        val uuid = try {
            UUID.fromString(id)
        } catch (e: IllegalArgumentException) {
            return null
        }
        if (!tracks.containsKey(uuid)) {
            return null
        }
        tracks.remove(UUID.fromString(id))
        return id
    }

}