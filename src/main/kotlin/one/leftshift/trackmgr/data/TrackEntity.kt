package one.leftshift.trackmgr.data

import one.leftshift.trackmgr.model.LocationResource
import one.leftshift.trackmgr.model.TrackResource
import java.time.OffsetDateTime
import java.util.*

/**
 * Represents a database entity
 * @author benjamin.krenn@leftshift.one - 10/8/21.
 * @since 1.0.0
 */
data class TrackEntity(
    val name: String,
    val longitude: Double,
    val latitude: Double,
    val createdAt: OffsetDateTime,
    val updatedAt: OffsetDateTime? = null,
    val id: UUID = UUID.nameUUIDFromBytes(name.toByteArray())
) {
    fun toResource(): TrackResource = TrackResource().let { resource: TrackResource ->
        resource.id = this.id.toString()
        resource.name = this.name
        resource.location = LocationResource().let { locationResource ->
            locationResource.latitude = this.latitude
            locationResource.longitude = this.longitude
            locationResource
        }
        resource.updatedAt = this.updatedAt
        resource.createdAt = this.createdAt
        resource
    }
}
