import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.5.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
    id("io.openapiprocessor.openapi-processor") version "2021.3"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}


/**
 * openapi processor configuration
 */
sourceSets {
    named("main") {
        this.java.srcDir("src/main/generated")
    }
}
openapiProcessor {
    // the path to the open api yaml file.
    apiPath("${rootDir}/openapi.yaml")

    process("spring") {
        processor("io.openapiprocessor:openapi-processor-spring:2021.5")
        targetDir("${projectDir}/src/main/generated/")
        prop("mapping", "${rootDir}/mapping.yaml")
    }

    process("json") {
        processor("io.openapiprocessor:openapi-processor-json:2021.2")
        targetDir("${buildDir}/resources/main/static")
    }
}